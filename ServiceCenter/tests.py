#! python
import unittest
import json
from datetime import datetime
from app import create_app, db
from config import Config


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client
        self.card = {
            "card_equipment_number": "M412S54A",
            "card_equipment_name": "Fridge",
            "card_user_name": "Дмитрий Терещенко Владиславович",
            "card_user_phone": "89422341123"
        }
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_build_create(self):
        res = self.client().post('/api/cards', data=json.dumps(self.card), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 201)
        self.assertIn(self.card['card_equipment_number'], str(res.data))

    def test_get_all_cards(self):
        res = self.client().post('/api/cards', data=json.dumps(self.card), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 201)
        res = self.client().get('/api/cards', headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 200)
        self.assertIn(self.card['card_equipment_number'], str(res.data))

    def test_get_card_by_id(self):
        rv = self.client().post('/api/cards', data=json.dumps(self.card), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'","\""))
        result = self.client().get('/api/cards/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 200)
        self.assertIn(self.card['card_equipment_number'], str(result.data))

    def test_card_can_be_edited(self):
        rv = self.client().post('/api/cards', data=json.dumps(self.card), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        card_user_phone = self.card['card_user_phone']
        self.card['card_user_phone'] = '89541235423'

        res = self.client().put('/api/cards/{}'.format(result_in_json['id']), data=json.dumps(self.card), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 200)

        result = self.client().get('/api/cards/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertIn(self.card['card_user_phone'], str(result.data))
        self.assertNotIn(card_user_phone, str(result.data))

    def test_build_deletion(self):
        rv = self.client().post('/api/cards', data=json.dumps(self.card), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        result = self.client().delete('/api/cards/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 200)

        result = self.client().get('/api/cards/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 404)

    def test_put_empty_object(self):
        rv = self.client().post('/api/cards', data=json.dumps(self.card), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        build_upd = {}

        res = self.client().put('/api/cards/{}'.format(result_in_json['id']), data=json.dumps(build_upd), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 400)
        self.assertIn('Build should contain something', str(res.data))

        result = self.client().get('/api/cards/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertIn(self.card['card_equipment_number'], str(result.data))

    def test_post_empty_object(self):
        res = self.client().post('/api/cards', data=json.dumps({}), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 400)
        self.assertIn('Card should contain something', str(res.data))


if __name__ == "__main__":
    unittest.main()
