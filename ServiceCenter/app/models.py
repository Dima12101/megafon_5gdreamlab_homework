from enum import Enum
from app import db
from flask import url_for


class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page, **kwargs),
                'next': url_for(endpoint, page=page + 1, per_page=per_page, **kwargs) if resources.has_next else None,
                'prev': url_for(endpoint, page=page - 1, per_page=per_page, **kwargs) if resources.has_prev else None
            }
        }
        return data


class JsonMixin(object):
    def to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def from_dict(self, data):
        for c in self.__table__.columns:
            if c.name in data:
                setattr(self, c.name, data[c.name])


class Card(db.Model, PaginatedAPIMixin, JsonMixin):
    __tablename__ = 'Cards'
    # Уникальный числовой идентификатор
    id = db.Column(db.Integer, primary_key=True)
    # Серийный номер оборудования (строка максимум 20 символов)
    card_equipment_number = db.Column(db.String(20))
    # Наименование оборудования (строка максимум 50 символов)
    card_equipment_name = db.Column(db.String(50))
    # ФИО сдавшего оборудование (строка максимум 50 символов)
    card_user_name = db.Column(db.String(50))
    # Контактный телефон (строка максимум 50 символов)
    card_user_phone = db.Column(db.String(50))
